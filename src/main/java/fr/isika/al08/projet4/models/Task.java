package fr.isika.al08.projet4.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long taskId;

    @ManyToOne(fetch= FetchType.LAZY)
    private User user;

    @NotBlank
    @Size(max = 50)
    private String title;

    @NotBlank
    @Size(max = 150)
    private String description;

    @NotBlank
    @Enumerated(value = EnumType.STRING)
    private Status status;
}
