package fr.isika.al08.projet4.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
