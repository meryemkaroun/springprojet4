package fr.isika.al08.projet4.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "favoriteoffers")
public class FavoriteOffer {

    @Id
    private String offerId;

    @ManyToOne(fetch= FetchType.LAZY)
    private User user;

}
