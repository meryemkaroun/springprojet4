package fr.isika.al08.projet4.security.services;

import fr.isika.al08.projet4.models.FavoriteOffer;
import fr.isika.al08.projet4.models.User;
import fr.isika.al08.projet4.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FavoriteOfferService {

    final UserRepository userRepository;

    public FavoriteOfferService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<FavoriteOffer> getFavoriteOffers(Long userId){
        return userRepository.findById(userId)
                .map(user -> user.getOffersFavory())
                .orElse(new ArrayList<>());
    }

    public void addFavoriteOffer(String offerId, Long userId) {
        User user = this.userRepository.findById(userId).orElseThrow();
        FavoriteOffer favoriteOffer = new FavoriteOffer(offerId, user);
        user.getOffersFavory().add(favoriteOffer);
        userRepository.save(user);
    }

    public void deleteFavoriteOffers(String offerId, Long userId) {
        User user = this.userRepository.findById(userId).orElseThrow();
        user.getOffersFavory().removeIf(offer -> offer.getOfferId().equals(offerId));
        userRepository.save(user);
    }
}
