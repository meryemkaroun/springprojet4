package fr.isika.al08.projet4.security.services;

import fr.isika.al08.projet4.models.Task;
import fr.isika.al08.projet4.models.User;
import fr.isika.al08.projet4.repositories.TaskRepository;
import fr.isika.al08.projet4.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService {

    final UserRepository userRepository;
    final TaskRepository taskRepository;

    public TaskService(UserRepository userRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    public List<Task> getTasks(Long userId) {
        return userRepository.findById(userId)
                .map(user -> user.getListOfTasks())
                .orElse(new ArrayList<>());
    }

    public Task addTask(Task task, Long userId) {
        User user = this.userRepository.findById(userId).orElseThrow();
        task.setUser(user);
        return taskRepository.save(task);
    }

    public Task updateTask(Task task, Long userId) {
        return taskRepository.save(task);
    }

    public void deleteTask(Long taskId, Long userId) {
        taskRepository.findById(taskId).ifPresent(task -> taskRepository.delete(task));
    }
}
