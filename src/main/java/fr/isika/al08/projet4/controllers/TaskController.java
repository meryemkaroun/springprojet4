package fr.isika.al08.projet4.controllers;

import fr.isika.al08.projet4.models.Task;
import fr.isika.al08.projet4.security.services.TaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/tasks")
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping("/{userId}/count")
    public ResponseEntity<Integer> getTaskCount (@PathVariable Long userId) {
        int taskCount = taskService.getTasks(userId).size();
        return ResponseEntity.ok(taskCount);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<List<Task>> getTask (@PathVariable Long userId) {
        List<Task> taskList = taskService.getTasks(userId);
        return ResponseEntity.ok(taskList);
    }

    @PostMapping("/{userId}")
    public ResponseEntity<Task> addTask(@PathVariable("userId") Long userId, @RequestBody  Task task) {
        return ResponseEntity.ok(taskService.addTask(task, userId));
    }

    @PutMapping("/{userId}")
    public ResponseEntity<Task> updateTask(@PathVariable("userId") Long userId, @RequestBody  Task task) {
        return ResponseEntity.ok(this.taskService.updateTask(task, userId));
    }

    @DeleteMapping("/{userId}/{taskId}")
    public void deleteTask(@PathVariable("userId") Long userId, @PathVariable("taskId") Long taskId) {
        this.taskService.deleteTask(taskId, userId);
    }
}
