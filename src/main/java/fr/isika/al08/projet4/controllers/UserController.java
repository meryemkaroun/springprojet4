package fr.isika.al08.projet4.controllers;

import fr.isika.al08.projet4.dto.UserDTO;
import fr.isika.al08.projet4.models.User;
import fr.isika.al08.projet4.security.services.UserDetailsImpl;
import fr.isika.al08.projet4.security.services.UserDetailsServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserController {

    private UserDetailsServiceImpl userDetailsServiceImpl;
    private PasswordEncoder encoder;

    public UserController(UserDetailsServiceImpl userDetailsServiceImpl, PasswordEncoder encoder) {
        this.userDetailsServiceImpl = userDetailsServiceImpl;
        this.encoder = encoder;
    }

    @PostMapping("/profil")
    public void updateUserProfil(@RequestBody UserDTO user) {
        UserDetailsImpl connectedUser = (UserDetailsImpl) getCurrentConnectedUser();

        User userInDb = this.userDetailsServiceImpl.findById(user.getId()).orElseThrow();

        if (connectedUser.getId().equals(user.getId())) {
            userInDb.setFirstName(user.getFirstName());
            userInDb.setUsername(user.getUsername());
            userInDb.setEmail(user.getEmail());
            userInDb.setAddress(user.getAddress());
            userInDb.setLastName(user.getLastName());
            userInDb.setGender(user.getGender());
            userInDb.setTel(user.getTel());
            if(user.getPassword() != null && !user.getPassword().equals("")){
                userInDb.setPassword(encoder.encode(user.getPassword()));
            }
            this.userDetailsServiceImpl.updateUser(userInDb);
        }
    }

    @GetMapping("/profil")
    public ResponseEntity<UserDTO> getUserProfil() {

        UserDetails connectedUser = getCurrentConnectedUser();
        User userInDb = this.userDetailsServiceImpl.findByUsername(connectedUser.getUsername()).orElseThrow();

        UserDTO userDTO = new UserDTO();
        userDTO.setFirstName(userInDb.getFirstName());
        userDTO.setUsername(userInDb.getUsername());
        userDTO.setEmail(userInDb.getEmail());
        userDTO.setAddress(userInDb.getAddress());
        userDTO.setLastName(userInDb.getLastName());
        userDTO.setGender(userInDb.getGender());
        userDTO.setTel(userInDb.getTel());
        userDTO.setId(userInDb.getId());

        return ResponseEntity.ok(userDTO);
    }

    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable("userId") Long id) {
        UserDetails connectedUser = getCurrentConnectedUser();
        User user = this.userDetailsServiceImpl.findById(id).orElseThrow();

        if (connectedUser.getUsername().equals(user.getUsername())) {
            this.userDetailsServiceImpl.deleteUser(user);
        }
    }

    private UserDetails getCurrentConnectedUser() {
        return (UserDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
    }
}
