package fr.isika.al08.projet4.controllers;

import fr.isika.al08.projet4.models.FavoriteOffer;
import fr.isika.al08.projet4.security.services.FavoriteOfferService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/favorites")
public class FavoriteOfferController {

    private FavoriteOfferService  favoriteOfferService;

    public FavoriteOfferController(FavoriteOfferService favoriteOfferService) {
        this.favoriteOfferService = favoriteOfferService;
    }

    @GetMapping("/offers/{userId}/count")
    public ResponseEntity<Integer> getFavoriteOffersCount (@PathVariable Long userId) {
        int favoriteOffersCount = favoriteOfferService.getFavoriteOffers(userId).size();
        return ResponseEntity.ok(favoriteOffersCount);
    }

    @GetMapping("/offers/{userId}")
    public ResponseEntity<List<FavoriteOffer>> getFavoriteOffers (@PathVariable Long userId) {
        List<FavoriteOffer> favoriteOffers = favoriteOfferService.getFavoriteOffers(userId);
        return ResponseEntity.ok(favoriteOffers);
    }

    @GetMapping("/offers/{userId}/{offerId}/isFavorite")
    public ResponseEntity<Boolean> isOfferInFavoritesOne(@PathVariable Long userId, @PathVariable("offerId") String offerId) {
        List<FavoriteOffer> favoriteOffers = favoriteOfferService.getFavoriteOffers(userId);

        boolean isFind = false;
        for(FavoriteOffer offer : favoriteOffers) {
            if(offer.getOfferId().equals(offerId)) {
                isFind = true;
                break;
            }
        }

        return ResponseEntity.ok(isFind);
    }

    @PostMapping("/offers/{userId}/{offerId}")
    public void addFavoriteOffers(@PathVariable("userId") Long userId, @PathVariable("offerId") String offerId) {
        favoriteOfferService.addFavoriteOffer(offerId, userId);
    }

    @DeleteMapping("/offers/{userId}/{offerId}")
    public void deleteFavoriteOffers(@PathVariable("userId") Long userId, @PathVariable("offerId") String offerId) {
        this.favoriteOfferService.deleteFavoriteOffers(offerId, userId);
    }

}