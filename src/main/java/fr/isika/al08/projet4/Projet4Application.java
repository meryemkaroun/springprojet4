package fr.isika.al08.projet4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projet4Application {

	public static void main(String[] args) {
		SpringApplication.run(Projet4Application.class, args);
	}

}
